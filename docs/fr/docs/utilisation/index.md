# Utilisation de Mobilizon

---

## Comment utiliser Mobilizon ?

Avant tout : merci d'utiliser Mobilizon !
Cette documentation a pour objectif de vous aider dans les différentes options et fonctionnalités.

### Utilisateur et utilisatrice

  * [Comment créer un compte](utilisateur et utilisatrice/creation-compte.md)
  * [Comment créer un profil](utilisateur et utilisatrice/creation-profile.md)
  * [Les paramètres de votre compte](utilisateur et utilisatrice/parametres-compte.md)

### Événements

  * [Comment commenter un événement](evenements/commentaires-evenement.md)
  * [Comment créer un événement](evenements/creation-evenement.md)
  * [Gérer les participations à un événement](evenements/gerer-participations.md)
  * [Participer à un événement](evenements/participer-evenement.md)
  * [Partager, signaler, éditer un événement](evenements/actions-evenement.md)

### Groupes

  * [Comment créer un groupe](groupes/creation-groupe.md)
  * [Discussions dans un groupe](groupes/discussion-groupe.md)
  * [Gérer les invitations dans le groupe](groupes/invitations-groupe.md)
  * [Gérer les rôles](groupes/roles-groupe.md)
  * [Message public d'un groupe](groupes/message-public-groupe.md)
  * [Page d'un groupe](groupes/page-groupe.md)
  * [Ressources du groupe](groupes/ressources-groupe.md)
