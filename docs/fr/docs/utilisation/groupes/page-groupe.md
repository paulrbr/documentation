# Page d'un groupe

## Signaler un groupe

Pour signaler un groupe, vous devez&nbsp;:

  1. cliquer sur le bouton **⋅⋅⋅**
  * cliquer sur le bouton **Signalement**

    ![bouton de signalement](../../images/report-group-FR.png)

  * [Optionnel mais **recommandé**] renseigner la raison du signalement&nbsp;:
    ![modale de signalement](../../images/report-group-modal-FR.png)
