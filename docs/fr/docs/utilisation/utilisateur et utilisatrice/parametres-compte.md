# Les paramètres de votre compte

Vous pouvez accéder à vos paramètres en cliquant sur votre avatar puis sur **Mon compte**.

## Général

### Email

Vous pouvez modifier l'email que vous utilisez pour vous connecter à votre compte. Pour cela, vous devez le faire :

  1. cliquez sur votre avatar dans la barre supérieure
  * cliquez sur **Mon compte**
  * cliquez sur l'onglet **Général** dans la barre de gauche
  * entrez votre nouvel e-mail dans le champ **Nouvelle adresse e-mail**
  * entrez votre mot de passe dans le champ **Mot de passe**
  * cliquez sur le bouton **Changer mon adresse e-mail**.

!!! note
    Vous recevrez un email de confirmation.

### Mot de passe

  1. cliquez sur votre avatar dans la barre supérieure
  * cliquez sur **Mon compte**
  * cliquez sur l'onglet **Général** dans la barre de gauche
  * entrer votre **ancien** mot de passe (celui à changer) dans le champ **Ancien mot de passe**
  * entrer le nouveau dans le champ **Nouveau mot de passe**
  * cliquer sur **Modifier mon mot de passe**

### Suppression du compte

!!! danger
    Vous allez tout perdre. Les identités, les profils, les paramètres, les événements créés, les messages et les participations auront disparu à jamais.
    **Il n'y aura aucun moyen de récupérer vos données**.

Pour supprimer vos données vous devez&nbsp;:

  1. cliquez sur votre avatar dans la barre supérieure
  * cliquez sur **Mon compte**
  * cliquez sur l'onglet **Général** dans la barre de gauche
  * cliquer sur le bouton **Supprimer mon compte**
  * entrer votre mot de passe pour confirmer votre action
  * cliquer sur le bouton **Tout supprimer** (ou sur le bouton **Annuler** pour garder votre compte)

## Préférences

Vous pouvez modifier la langue de l'interface de votre compte et le fuseau horaire. Pour faire cela vous devez&nbsp;:

  1. cliquez sur votre avatar dans la barre supérieure
  * cliquez sur **Mon compte**
  * cliquez sur **Préférences** dans le menu latéral gauche
  * changer la langue ou/et le fuseau horaire

## Notifications par email

  1. cliquez sur votre avatar dans la barre supérieure
  * cliquez sur **Mon compte**
  * cliquez sur l'onglet **Notifications par email** dans la barre de gauche
  * choisir entre&nbsp;:
    * Notification le jour de l'événement
    * Récapitulatif hebdomadaire
    * Notification avant l'événement

### Notifications pour organisateur·ice

En tant qu'organisateur·ice, vous pouvez recevoir des notifications pour les participations à un événement nécessitant une approbation manuelle. Vous pouvez choisir entre&nbsp;:

  * Ne pas recevoir d'e-mail
  * Recevoir un e-mail par demande
  * E-mail récapitulatif chaque heure
  * E-mail récapitulatif chaque jour
