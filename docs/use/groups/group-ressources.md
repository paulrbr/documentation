# Group ressources

 A place to store links to documents or resources of any type.

 **Ressources** section allows you to add:

  * folder
  * web links
  * calc
  * pad
  * visioconference

![image add resources](../../images/en/group-add-ressources.png)

## Access to resources

To access to your group resources you have to be **at least** member of this group. Then, you have to:

  1. click **My groups** available in the top menu
  * click the group you want to
  * click **View all**

    ![title resources section](../../images/en/group-resource-title-EN.png)

## Add a resource

To add a resource you have to, [in resources page](#access-to-resources), click on **+** button and choose what kind of resource you want to add:

![image add resources](../../images/en/group-add-ressources.png)

### Add a new folder

To add a new folder you have to, [in resources page](#access-to-resources):

  1. [click **+** button](#add-a-resource) (see above)
  * choose **New folder**
  * add a name to this folder
  * click **Create a folder** button

### Add a new link

To add a new link you have to, [in resources page](#access-to-resources):

  1. [click **+** button](#add-a-resource) (see above)
  * choose **New link**
  * add the web link
  * [optional] add a title (this will be automatically added, otherwise)
  * [optional] add a description (this will be automatically added, otherwise)
  * click **Create resource** button

### Add a calc

To add a new calc you have to, [in resources page](#access-to-resources):

  1. [click **+** button](#add-a-resource) (see above)
  * choose **Create a calc**
  * choose a name for this calc
  * click **Create a calc** button

!!! note
    This calc will be create on a external website (configurable by the instance manager)

### Add a pad

To add a new pad you have to, [in resources page](#access-to-resources):

  1. [click **+** button](#add-a-resource) (see above)
  * choose **Create a pad**
  * choose a name for this pad
  * click **Create a pad** button

!!! note
    This pad will be create on a external website (configurable by the instance manager)

### Add a visioconference

To add a new visioconference you have to, [in resources page](#access-to-resources):

  1. [click **+** button](#add-a-resource) (see above)
  * choose **Create a visioconference**
  * choose a name for this visioconference
  * click **Create a visioconference** button

!!! note
    This visioconference will be create on a external website (configurable by the instance manager)

## Rename a resource

To rename a folder or a file, you have to, [in resources page](#access-to-resources):

  1. click **⋅⋅⋅** in front of the resource
  * click <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
    </svg> **Rename**
  * make changes
  * click **Rename resource** button

## Move a resource

You can move a file into a folder, or a folder into another one, etc… To do so, you have to, [in resources page](#access-to-resources):

  1. click **⋅⋅⋅** in front of the resource
  * click <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="currentColor" d="M14,18V15H10V11H14V8L19,13M20,6H12L10,4H4C2.89,4 2,4.89 2,6V18A2,2 0 0,0 4,20H20A2,2 0 0,0 22,18V8C22,6.89 21.1,6 20,6Z" /></svg> **Move**
  * choose where you want to move your resource
  * once in the right place, click **Move resource to [FOLDER]** button

![gif showing file moving into a folder](../../images/en/move-resource-EN.gif)

## Delete a resource

To delete a resource tou have to, [in resources page](#access-to-resources):

  1. click **⋅⋅⋅** in front of the resource
  * click <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" /></svg> **Delete**
